package com.techgeeknext.controller;
//import com.techgeeknext.dao.ProfileDao;
import com.techgeeknext.dao.*;
//import com.techgeeknext.repository.ProfileRepository;
import com.techgeeknext.dto.*;
import com.techgeeknext.repository.*;
import com.techgeeknext.service.*;
import com.techgeeknext.util.CustomErrorType;
import com.techgeeknext.util.CustomSuccessType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class NulocaController {
    @Autowired
    UserService userDetailsService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    ProductService productService;
    @Autowired
    ProductDetailService productDetailService;
    @Autowired
    TransactionService transactionService;
    @Autowired
    EwalletService ewalletService;

    //============================================= TESTING =================================================

    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public String getEmployees() {
        return "Welcome!";
    }

    //========================================== USER CONTROLLER ============================================

    /**
     * Note to Solve (There are some bugs)
     * User can open, update and delete other user account by Id_User.
     * It may happen because programmer was not set session management in a proper way.
     */

    //==== ROLE AS USER
    // Open User Profile
    @GetMapping("/user/{id}")
    public ResponseEntity<?> getUserProfile(@PathVariable("id") int idUser) {
        try {
            UserDao user = userDetailsService.getUserById(idUser);
            if (user == null) {
                return new ResponseEntity<>(new CustomErrorType("User not found."), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(user, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Update User Profile
    @PutMapping("/user/{id}")
    public ResponseEntity<?> updateUserProfile(@PathVariable("id") int idUser, @RequestBody UserDto updateUser) {
        try {
            UserDao user = userDetailsService.getUserById(idUser);

            if (user == null) {
                return new ResponseEntity<>(new CustomErrorType("User not found."), HttpStatus.NOT_FOUND);
            } else {
                UserDao userUpdated = userDetailsService.updateUser(user, updateUser);
                return new ResponseEntity<>(userUpdated, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Account User
    @DeleteMapping("/user/{id}")
    public ResponseEntity<?> deleteUserAccount(@PathVariable("id") int id_user) {
        try {
            UserDao user = userDetailsService.getUserById(id_user);

            if (user == null) {
                return new ResponseEntity<>(new CustomErrorType("User not found."), HttpStatus.NOT_FOUND);
            } else {
                try {
                    userDetailsService.deleteUser(user);
                    return new ResponseEntity<>(new CustomSuccessType("Succeed to delete User."), HttpStatus.NO_CONTENT);
                } catch (Exception e) {
                    return new ResponseEntity<>(new CustomErrorType("Failed to delete User."), HttpStatus.EXPECTATION_FAILED);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //==== ROLE AS ADMIN
    // Open All User Profile
    @GetMapping("/user/")
    public ResponseEntity<?> getUserProfileList() {
        try {
            List<UserDao> userList = userDetailsService.getAllUser();
            if (userList.isEmpty()) {
                return new ResponseEntity<>(new CustomErrorType("Database User is empty."), HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(userList, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //========================================== HOME CONTROLLER ============================================

    //==== ROLE AS USER
    // Open All Category in Tagging Product on Home
    @GetMapping("/category/")
    public ResponseEntity<?> getCategoryList() {
        try {
            List<CategoryDao> categoryList = categoryService.getAllCategory();
            if (categoryList.isEmpty()) {
                return new ResponseEntity<>(new CustomErrorType("Database User is empty."), HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(categoryList, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Open All Product on Home Preview
    @GetMapping("/product/")
    public ResponseEntity<?> getProductList() {
        try {
            List<ProductDao> productList = productService.getAllProduct();
            if (productList.isEmpty()) {
                return new ResponseEntity<>(new CustomErrorType("Database User is empty."), HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(productList, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //==== ROLE AS ADMIN
    // Insert Category in Tagging Product
    @PostMapping("/category/")
    public ResponseEntity<?> insertCategory(@RequestBody CategoryDto category) {
        try {
            CategoryDao categoryAddedd = categoryService.insertCategory(category);
            return new ResponseEntity<>(categoryAddedd, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Update Category Tagging Product
    @PutMapping("/category/{id}")
    public ResponseEntity<?> updateCategory(@PathVariable("id") int id_category, @RequestBody CategoryDto updateCategory) {
        try {
            CategoryDao category = categoryService.getCategoryById(id_category);

            if (category == null) {
                return new ResponseEntity<>(new CustomErrorType("Category not found."), HttpStatus.NOT_FOUND);
            } else {
                CategoryDao categoryUpdated = categoryService.updateCategory(category, updateCategory);
                return new ResponseEntity<>(categoryUpdated, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Category Tagging Product
    @DeleteMapping("/category/{id}")
    public ResponseEntity<?> deleteCategory(@PathVariable("id") int id_category) {
        try {
            CategoryDao category = categoryService.getCategoryById(id_category);

            if (category == null) {
                return new ResponseEntity<>(new CustomErrorType("Category not found."), HttpStatus.NOT_FOUND);
            } else {
                try {
                    categoryService.deleteCategory(category);
                    return new ResponseEntity<>(new CustomSuccessType("Succeed to delete Category."), HttpStatus.NO_CONTENT);
                } catch (Exception e) {
                    return new ResponseEntity<>(new CustomErrorType("Failed to delete Category."), HttpStatus.EXPECTATION_FAILED);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Insert Product
    @PostMapping("/product/")
    public ResponseEntity<?> insertProduct(@RequestBody ProductDto product) {
        try {
            ProductDao productAddedd = productService.insertProduct(product);
            return new ResponseEntity<>(productAddedd, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Update Product
    @PutMapping("/product/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable("id") int idProduct, @RequestBody ProductDto updateProduct) {
        try {
            ProductDao product = productService.getProductById(idProduct);

            if (product == null) {
                return new ResponseEntity<>(new CustomErrorType("Product not found."), HttpStatus.NOT_FOUND);
            } else {
                ProductDao productUpdated = productService.updateProduct(product, updateProduct);
                return new ResponseEntity<>(productUpdated, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Product
    @DeleteMapping("/product/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int idProduct) {
        try {
            ProductDao product = productService.getProductById(idProduct);

            if (product == null) {
                return new ResponseEntity<>(new CustomErrorType("Product not found."), HttpStatus.NOT_FOUND);
            } else {
                try {
                    productService.deleteProduct(product);
                    return new ResponseEntity<>(new CustomSuccessType("Succeed to delete Product."), HttpStatus.NO_CONTENT);
                } catch (Exception e) {
                    return new ResponseEntity<>(new CustomErrorType("Failed to delete Product."), HttpStatus.EXPECTATION_FAILED);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //========================================== CATEGORY DETAILS PAGE CONTROLLER ============================================

    /**
     * Should make API for fetch product list.
     * It must be contained array of products.
     * HINT : Related to category and product tables.
     * (GET "/category/{id}")
     * GETTING DONE!
     */

    //==== ROLE AS USER
    // Open All Category in List by id_category
    @GetMapping("/category/{id}")
    public ResponseEntity<?> getProductList(@PathVariable("id") int idCategory) {
        try {
            CategoryDao category = categoryService.getCategoryById(idCategory);
            if (category == null) {
                return new ResponseEntity<>(new CustomErrorType("Category not found."), HttpStatus.NOT_FOUND);
            } else {
                List<ProductDao> productList = productService.getproductListinCategory(idCategory);
                ArrayList nameProduct = new ArrayList();

                if (productList == null) {
                    return new ResponseEntity<>(new CustomErrorType("Product List in Category not found."), HttpStatus.NOT_FOUND);
                } else {
                    for (ProductDao getNameProducts : productList) {
                        nameProduct.add(getNameProducts.getNameProduct());
                    }
                    CategoryDto productListinCategory = new CategoryDto();
                    productListinCategory.setNameCategory(category.getNameCategory());
                    productListinCategory.setProductList(nameProduct);

                    return new ResponseEntity<>(productListinCategory, HttpStatus.OK);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //========================================== PRODUCT DETAILS PAGE CONTROLLER ============================================

    /**
     * Should make API for fetch product details.
     * It must be contained array of productsdetails.
     * HINT : Related to product and productdetail tables.
     * (GET "/product/{id}")
     */

    //==== ROLE AS USER
    // Open All ProductDetail in List by id_product
    @GetMapping("/product/{id}")
    public ResponseEntity<?> getProductDetailList(@PathVariable("id") int idProduct) {
        try {
            ProductDao product = productService.getProductById(idProduct);
            if (product == null) {
                return new ResponseEntity<>(new CustomErrorType("Product not found."), HttpStatus.NOT_FOUND);
            } else {
                List<ProductDetailDao> productDetailList = productDetailService.getproductDetailListinProduct(idProduct);
                List<ProductDetailDto> productDetailArr = new ArrayList();

                if (productDetailList == null) {
                    return new ResponseEntity<>(new CustomErrorType("Product Detail List in Product not found."), HttpStatus.NOT_FOUND);
                } else {
                    for (ProductDetailDao getProductDetailsArr : productDetailList) {
                        ProductDetailDto productDetail = new ProductDetailDto();
                        productDetail.setNameProductDetail(getProductDetailsArr.getNameProductDetail());
                        productDetail.setPrice(getProductDetailsArr.getPrice());
                        productDetailArr.add(productDetail);
                    }
                    ProductDto productDetailListinProduct = new ProductDto();
                    productDetailListinProduct.setNameProduct(product.getNameProduct());
                    productDetailListinProduct.setProductDetailList(productDetailArr);

                    return new ResponseEntity<>(productDetailListinProduct, HttpStatus.OK);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //==== ROLE AS ADMIN
    // Insert ProductDetail
    @PostMapping("/product/detail/")
    public ResponseEntity<?> insertProductDetail(@RequestBody ProductDetailDto productDetail) {
        try {
            ProductDetailDao productDetailAddedd = productDetailService.insertProductDetail(productDetail);
            return new ResponseEntity<>(productDetailAddedd, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Update ProductDetail
    @PutMapping("/product/detail/{id}")
    public ResponseEntity<?> updateProductDetail(@PathVariable("id") int idProductDetail, @RequestBody ProductDetailDto updateProductDetail) {
        try {
            ProductDetailDao productDetail = productDetailService.getProductDetailById(idProductDetail);

            if (productDetail == null) {
                return new ResponseEntity<>(new CustomErrorType("ProductDetail not found."), HttpStatus.NOT_FOUND);
            } else {
                ProductDetailDao productDetailUpdated = productDetailService.updateProductDetail(productDetail, updateProductDetail);
                return new ResponseEntity<>(productDetailUpdated, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete ProductDetail
    @DeleteMapping("/product/detail/{id}")
    public ResponseEntity<?> deleteProductDetail(@PathVariable("id") int idProductDetail) {
        try {
            ProductDetailDao productDetail = productDetailService.getProductDetailById(idProductDetail);

            if (productDetail == null) {
                return new ResponseEntity<>(new CustomErrorType("Product Detail not found."), HttpStatus.NOT_FOUND);
            } else {
                try {
                    productDetailService.deleteProductDetail(productDetail);
                    return new ResponseEntity<>(new CustomSuccessType("Succeed to delete Product Detail."), HttpStatus.NO_CONTENT);
                } catch (Exception e) {
                    return new ResponseEntity<>(new CustomErrorType("Failed to delete Product Detail."), HttpStatus.EXPECTATION_FAILED);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //========================================== MY SUBSCRIPTIONS CONTROLLER ============================================

    /**
     * Should make API for fetch user's subscription products.
     * It must be contained array of product.
     * HINT : Related to user, userproduct and product tables.
     * (GET "/user/subscription/{id}")
     */

    //========================================== TRANSACTION CONTROLLER ============================================

    //==== ROLE USER
    // Insert Transaction
    @PostMapping("/payment/")
    public ResponseEntity<?> insertTransactionDetail(@RequestBody TransactionDto transaction) {
        try {
            TransactionDao transactionAddedd = transactionService.insertTransaction(transaction);
            return new ResponseEntity<>(transactionAddedd, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Show Transaction
    @GetMapping("/payment/{id}")
    public ResponseEntity<?> getTransaction(@PathVariable("id") int idTransaction) {
        try {
            TransactionDao transaction = transactionService.getTransactionById(idTransaction);
            if (transaction == null) {
                return new ResponseEntity<>(new CustomErrorType("Transaction not found."), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(transaction, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//    try {
//        ProductDao product = productService.getProductById(idProduct);
//        if (product == null) {
//            return new ResponseEntity<>(new CustomErrorType("Product not found."), HttpStatus.NOT_FOUND);
//        } else {
//            List<ProductDetailDao> productDetailList = productDetailService.getproductDetailListinProduct(idProduct);
//            List<ProductDetailDto> productDetailArr = new ArrayList();
//
//            if (productDetailList == null) {
//                return new ResponseEntity<>(new CustomErrorType("Product Detail List in Product not found."), HttpStatus.NOT_FOUND);
//            } else {
//                for(ProductDetailDao getProductDetailsArr : productDetailList){
//                    ProductDetailDto productDetail = new ProductDetailDto();
//                    productDetail.setNameProductDetail(getProductDetailsArr.getNameProductDetail());
//                    productDetail.setPrice(getProductDetailsArr.getPrice());
//                    productDetailArr.add(productDetail);
//                }
//                ProductDto productDetailListinProduct = new ProductDto();
//                productDetailListinProduct.setNameProduct(product.getNameProduct());
//                productDetailListinProduct.setProductDetailList(productDetailArr);
//
//                return new ResponseEntity<>(productDetailListinProduct, HttpStatus.OK);
//            }
//        }
//    } catch (Exception e) {
//        return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
//    }

//    // Open Transaction by IdTransaction
//    @GetMapping("/user/payment/{id}")
//    public ResponseEntity<?> getTransactionById(@PathVariable("id") int idUser){
//        try{
//            UserDao userTransaction = userDetailsService.getUserById(idUser);
//            if(userTransaction==null){
//                return new ResponseEntity<>(new CustomErrorType("Transaction not found."), HttpStatus.NOT_FOUND);
//            }else{
//                List<TransactionDao> transactionList = transactionService.getUserListinTransaction(idUser);
//                List<TransactionDto> transactionObjinArray = new ArrayList();
//
//                if(transactionList==null){
//                    return new ResponseEntity<>(new CustomErrorType("Transaction List in User not found."), HttpStatus.NOT_FOUND);
//                }else{
//                    for(TransactionDao getTransactionsArr : transactionList){
//                        TransactionDto transaction = new TransactionDto();
//                        transaction.setAmountTransaction(getTransactionsArr.getAmountTransaction());
//                    }
//                }
//            }
//        }
//    }

    //==== ROLE ADMIN
    // Open All Transaction Users
    @GetMapping("/payment/")
    public ResponseEntity<?> getTransactionList() {
        try {
            List<TransactionDao> transactionList = transactionService.getAllTransaction();
            if (transactionList.isEmpty()) {
                return new ResponseEntity<>(new CustomErrorType("Database Transaction is empty."), HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(transactionList, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Update Transaction
    @PutMapping("/payment/{id}")
    public ResponseEntity<?> updateTransaction(@PathVariable("id") int idTransaction, @RequestBody TransactionDto updateTransaction) {
        try {
            TransactionDao transaction = transactionService.getTransactionById(idTransaction);

            if (transaction == null) {
                return new ResponseEntity<>(new CustomErrorType("Transaction not found."), HttpStatus.NOT_FOUND);
            } else {
                TransactionDao transactionUpdated = transactionService.updateTransaction(transaction, updateTransaction);
                return new ResponseEntity<>(transactionUpdated, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete ProductDetail
    @DeleteMapping("/payment/{id}")
    public ResponseEntity<?> deleteTransaction(@PathVariable("id") int idTransaction) {
        try {
            TransactionDao transaction = transactionService.getTransactionById(idTransaction);

            if (transaction == null) {
                return new ResponseEntity<>(new CustomErrorType("Transaction not found."), HttpStatus.NOT_FOUND);
            } else {
                try {
                    transactionService.deleteTransaction(transaction);
                    return new ResponseEntity<>(new CustomSuccessType("Succeed to delete Transaction."), HttpStatus.NO_CONTENT);
                } catch (Exception e) {
                    return new ResponseEntity<>(new CustomErrorType("Failed to delete Transaction."), HttpStatus.EXPECTATION_FAILED);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //========================================== EWALLET CONTROLLER ============================================

    //==== ROLE USER
    /**
     * Have not yet decided to push user TopUp their Ewallet (initiate insert ewallet in table) to shop in NULOCA.
     * HINT : Related to ewallet tables.
     * (POST "/ewallet/")
     */
    // Insert Balance Ewallet First to Shop in NULOCA
    @PostMapping("/ewallet/")
    public ResponseEntity<?> insertEwallet(@RequestBody EwalletDto ewallet) {
        try {
            EwalletDao ewalletAddedd = ewalletService.insertEwallet(ewallet);
            return new ResponseEntity<>(ewalletAddedd, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.EXPECTATION_FAILED);
        }
    }

    // TopUp Ewallet
    @PutMapping("/ewallet/topUp/{id}")
    public ResponseEntity<?> topUp(@PathVariable("id") int idEwallet, @RequestBody EwalletDto topUp) {
        try {
            EwalletDao ewallet = ewalletService.getEwalletById(idEwallet);

            if (ewallet == null) {
                return new ResponseEntity<>(new CustomErrorType("Ewallet not found."), HttpStatus.NOT_FOUND);
            } else {
                EwalletDao ewalletTopUp = ewalletService.topUpEwallet(ewallet, topUp);
                return new ResponseEntity<>(ewalletTopUp, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Payment Ewallet
    @PutMapping("/ewallet/payment/{id}")
    public ResponseEntity<?> payment(@PathVariable("id") int idEwallet, @RequestBody EwalletDto payment) {
        try {
            EwalletDao ewallet = ewalletService.getEwalletById(idEwallet);

            if (ewallet == null) {
                return new ResponseEntity<>(new CustomErrorType("Ewallet not found."), HttpStatus.NOT_FOUND);
            } else {
                EwalletDao ewalletPayment = ewalletService.paymentEwallet(ewallet, payment);
                return new ResponseEntity<>(ewalletPayment, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //==== ROLE ADMIN
    // Open All Ewallet Users
    @GetMapping("/ewallet/")
    public ResponseEntity<?> getEwalletList() {
        try {
            List<EwalletDao> ewalletList = ewalletService.getAllEwallet();
            if (ewalletList.isEmpty()) {
                return new ResponseEntity<>(new CustomErrorType("Database Ewallet is empty."), HttpStatus.NO_CONTENT);
            } else {
                return new ResponseEntity<>(ewalletList, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Update Ewallet
    @PutMapping("/ewallet/{id}")
    public ResponseEntity<?> updateEwallet(@PathVariable("id") int idEwallet, @RequestBody EwalletDto updateEwallet) {
        try{
            EwalletDao ewallet = ewalletService.getEwalletById(idEwallet);
            if (ewallet==null) {
                return new ResponseEntity<>(new CustomErrorType("Ewallet not found."), HttpStatus.NOT_FOUND);
            } else {
                EwalletDao currentBalance = ewalletService.updateEwallet(ewallet, updateEwallet);
                return new ResponseEntity<>(currentBalance, HttpStatus.OK);
            }
        }catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Delete Ewallet
    @DeleteMapping("/ewallet/{id}")
    public ResponseEntity<?> deleteEwallet(@PathVariable("id") int idEwallet) {
        try {
            EwalletDao ewallet = ewalletService.getEwalletById(idEwallet);

            if (ewallet == null) {
                return new ResponseEntity<>(new CustomErrorType("Ewallet not found."), HttpStatus.NOT_FOUND);
            } else {
                try {
                    ewalletService.deleteEwallet(ewallet);
                    return new ResponseEntity<>(new CustomSuccessType("Succeed to delete Ewallet."), HttpStatus.NO_CONTENT);
                } catch (Exception e) {
                    return new ResponseEntity<>(new CustomErrorType("Failed to delete Ewallet."), HttpStatus.EXPECTATION_FAILED);
                }
            }
        } catch (Exception e) {
            return new ResponseEntity<>(new CustomErrorType("Error was occurred in internal server."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
