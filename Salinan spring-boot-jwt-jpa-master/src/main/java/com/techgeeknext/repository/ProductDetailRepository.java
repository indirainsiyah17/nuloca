package com.techgeeknext.repository;

import com.techgeeknext.dao.CategoryDao;
import com.techgeeknext.dao.ProductDetailDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDetailRepository extends CrudRepository<ProductDetailDao, Integer> {
    ProductDetailDao findByIdProductDetail(int idProductDetail);
    List<ProductDetailDao> findAllByIdProduct(int idProduct);
    List<ProductDetailDao> findAll();
}
