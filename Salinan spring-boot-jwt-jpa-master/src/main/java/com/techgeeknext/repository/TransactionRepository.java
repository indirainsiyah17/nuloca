package com.techgeeknext.repository;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dao.TransactionDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionDao, Integer> {
    TransactionDao findByIdTransaction(int idTransaction);
    List<TransactionDao> findAll();
    List<TransactionDao> findAllByIdUserTrans(int idUserTrans);
    List<TransactionDao> findAllByIdProductTrans(int idProductTrans);
    List<TransactionDao> findAllByIdProductDetailTrans(int idProductDetailTrans);
}
