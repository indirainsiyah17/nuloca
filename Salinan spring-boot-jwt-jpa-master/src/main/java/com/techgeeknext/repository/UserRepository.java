package com.techgeeknext.repository;
import com.techgeeknext.dao.UserDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<UserDao, Integer> {
    UserDao findByIdUser(int idUser);
    UserDao findByUsername(String username);
    List<UserDao> findAll();
}