package com.techgeeknext.repository;

import com.techgeeknext.dao.ProductDao;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import javax.persistence.Entity;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface ProductRepository extends CrudRepository<ProductDao, Integer> {
//    @Modifying
//    @Query(value = "SELECT p.nameProduct FROM product p WHERE p.idCategory=:id",nativeQuery = true)
//    List<ProductDao> productList(@Param("id") int idCategory);
    //List<ProductDao> productList(@Param("id") int id_category);WHERE p.id_category=:id
    ProductDao findByIdProduct(int idProduct);
    List<ProductDao> findAllByIdCategory(int idCategory);
    List<ProductDao> findAll();
}
