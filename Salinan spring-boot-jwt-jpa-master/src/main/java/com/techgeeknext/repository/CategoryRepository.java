package com.techgeeknext.repository;

import com.techgeeknext.dao.CategoryDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends CrudRepository<CategoryDao, Integer> {
    CategoryDao findByIdCategory(int idCategory);
    List<CategoryDao> findAll();
}
