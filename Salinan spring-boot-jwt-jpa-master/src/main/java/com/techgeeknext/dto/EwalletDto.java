package com.techgeeknext.dto;

import java.io.Serializable;

public class EwalletDto implements Serializable {
    private int idEwallet;
    private long balance;
    private int idUserEwallet;

    public int getIdEwallet() {
        return idEwallet;
    }

    public void setIdEwallet(int idEwallet) {
        this.idEwallet = idEwallet;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public int getIdUserEwallet() {
        return idUserEwallet;
    }

    public void setIdUserEwallet(int idUserEwallet) {
        this.idUserEwallet = idUserEwallet;
    }

    public void topUp(long balance){
        this.balance+=balance;
    }

    public void payment(long balance){
        this.balance-=balance;
    }
}
