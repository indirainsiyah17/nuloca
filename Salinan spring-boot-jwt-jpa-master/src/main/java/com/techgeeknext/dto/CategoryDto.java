package com.techgeeknext.dto;

import com.techgeeknext.dao.ProductDao;

import java.io.Serializable;
import java.util.List;

public class CategoryDto implements Serializable{
    private int idCategory;
    private String nameCategory;
    private List<ProductDao> productList;

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public List<ProductDao> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDao> productList) {
        this.productList = productList;
    }
}
