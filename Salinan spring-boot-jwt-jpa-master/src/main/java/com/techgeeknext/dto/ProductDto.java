package com.techgeeknext.dto;

import com.techgeeknext.dao.ProductDetailDao;

import java.io.Serializable;
import java.util.List;

public class ProductDto implements Serializable {
    private int idProduct;
    private String nameProduct;
    private int idCategory;
    private List<ProductDetailDto> productDetailList;

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public List<ProductDetailDto> getProductDetailList() {
        return productDetailList;
    }

    public void setProductDetailList(List<ProductDetailDto> productDetailList) {
        this.productDetailList = productDetailList;
    }
}
