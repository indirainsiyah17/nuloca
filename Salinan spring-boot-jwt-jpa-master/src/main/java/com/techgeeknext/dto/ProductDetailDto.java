package com.techgeeknext.dto;

import java.io.Serializable;

public class ProductDetailDto implements Serializable {
    private int idProductDetail;
    private String nameProductDetail;
    private int price;
    private int idProduct;

    public ProductDetailDto(){}

    public int getIdProductDetail() {
        return idProductDetail;
    }

    public void setIdProductDetail(int idProductDetail) {
        this.idProductDetail = idProductDetail;
    }

    public String getNameProductDetail() {
        return nameProductDetail;
    }

    public void setNameProductDetail(String nameProductDetail) {
        this.nameProductDetail = nameProductDetail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
}
