package com.techgeeknext.dao;

import javax.persistence.*;

@Entity
@Table(name = "product_detail")
public class ProductDetailDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idProductDetail;
    private String nameProductDetail;
    private int price;
    private int idProduct;

    public int getIdProductDetail() {
        return idProductDetail;
    }

    public void setIdProductDetail(int idProductDetail) {
        this.idProductDetail = idProductDetail;
    }

    public String getNameProductDetail() {
        return nameProductDetail;
    }

    public void setNameProductDetail(String nameProductDetail) {
        this.nameProductDetail = nameProductDetail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
}
