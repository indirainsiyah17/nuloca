package com.techgeeknext.dao;

import javax.persistence.*;

@Entity
@Table(name = "transaction")
public class TransactionDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTransaction;
    private long amountTransaction;
    private int idUserTrans;
    private int idProductTrans;
    private int idProductDetailTrans;

    public int getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(int idTransaction) {
        this.idTransaction = idTransaction;
    }

    public long getAmountTransaction() {
        return amountTransaction;
    }

    public void setAmountTransaction(long amountTransaction) {
        this.amountTransaction = amountTransaction;
    }

    public int getIdUserTrans() {
        return idUserTrans;
    }

    public void setIdUserTrans(int idUserTrans) {
        this.idUserTrans = idUserTrans;
    }

    public int getIdProductTrans() {
        return idProductTrans;
    }

    public void setIdProductTrans(int idProductTrans) {
        this.idProductTrans = idProductTrans;
    }

    public int getIdProductDetailTrans() {
        return idProductDetailTrans;
    }

    public void setIdProductDetailTrans(int idProductDetailTrans) {
        this.idProductDetailTrans = idProductDetailTrans;
    }

}
