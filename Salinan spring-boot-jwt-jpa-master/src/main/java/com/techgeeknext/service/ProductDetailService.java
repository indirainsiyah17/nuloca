package com.techgeeknext.service;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dao.ProductDetailDao;
import com.techgeeknext.dto.ProductDetailDto;
import com.techgeeknext.dto.ProductDto;
import com.techgeeknext.repository.ProductDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ProductDetailService {
    @Autowired
    private ProductDetailRepository productDetailRepository;

    //==== ROLE AS USER
    // Fetch Category by idCategory from Database
    public ProductDetailDao getProductDetailById(int idProductDetail) {
        return productDetailRepository.findByIdProductDetail(idProductDetail);
    }

    // Insert Product to Database
    public ProductDetailDao insertProductDetail(ProductDetailDto insertProductDetail){
        ProductDetailDao productDetail = new ProductDetailDao();
        productDetail.setNameProductDetail(insertProductDetail.getNameProductDetail());
        productDetail.setPrice(insertProductDetail.getPrice());
        productDetail.setIdProduct(insertProductDetail.getIdProduct());
        return productDetailRepository.save(productDetail);
    }

    // Update Category to Database
    public ProductDetailDao updateProductDetail(ProductDetailDao productDetail, ProductDetailDto updateProductDetail){
        productDetail.setNameProductDetail(updateProductDetail.getNameProductDetail());
        productDetail.setPrice(updateProductDetail.getPrice());
        productDetail.setIdProduct(updateProductDetail.getIdProduct());
        return productDetailRepository.save(productDetail);
    }

    // Delete Category from Database
    public void deleteProductDetail(ProductDetailDao productDetail){
        productDetailRepository.delete(productDetail);
    }


    //==== ROLE AS ADMIN
    // Fetch All Users from Database
    public List<ProductDetailDao> getAllProductDetail(){
        return productDetailRepository.findAll();
    }

    // Dependency to Category
    public List<ProductDetailDao> getproductDetailListinProduct(int idProduct){
        return productDetailRepository.findAllByIdProduct(idProduct);
    }
}
