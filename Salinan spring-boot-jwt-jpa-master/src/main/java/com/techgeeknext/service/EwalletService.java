package com.techgeeknext.service;

import com.techgeeknext.dao.EwalletDao;
import com.techgeeknext.dao.TransactionDao;
import com.techgeeknext.dto.EwalletDto;
import com.techgeeknext.dto.TransactionDto;
import com.techgeeknext.repository.EwalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class EwalletService {
    @Autowired
    EwalletRepository ewalletRepository;

    //==== ROLE AS USER
    // Fetch Category by idCategory from Database
    public EwalletDao getEwalletById(int idEwallet) {
        return ewalletRepository.findByIdEwallet(idEwallet);
    }

    // Insert Ewallet to Database
    public EwalletDao insertEwallet(EwalletDto currentEwallet){
        EwalletDao ewallet = new EwalletDao();
        ewallet.setBalance(currentEwallet.getBalance());
        ewallet.setIdUserEwallet(currentEwallet.getIdUserEwallet());
        return ewalletRepository.save(ewallet);
    }

    // Update Ewallet by TopUp to Database
    public EwalletDao topUpEwallet(EwalletDao topUp, EwalletDto currentEwallet){
        topUp.topUp(currentEwallet.getBalance());
        return ewalletRepository.save(topUp);
    }

    // Update Ewallet by Payment to Database
    public EwalletDao paymentEwallet(EwalletDao payment, EwalletDto currentEwallet){
        payment.payment(currentEwallet.getBalance());
        return ewalletRepository.save(payment);
    }

    // Update Category to Database
    public EwalletDao updateEwallet(EwalletDao updateEwallet, EwalletDto currentEwallet){
        updateEwallet.setBalance(currentEwallet.getBalance());
        return ewalletRepository.save(updateEwallet);
    }

    // Delete Category from Database
    public void deleteEwallet(EwalletDao ewallet){
        ewalletRepository.delete(ewallet);
    }


    //==== ROLE AS ADMIN
    // Fetch All Users from Database
    public List<EwalletDao> getAllEwallet(){
        return ewalletRepository.findAll();
    }
}
