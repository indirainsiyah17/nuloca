package com.techgeeknext.service;

import com.techgeeknext.dao.ProductDetailDao;
import com.techgeeknext.dao.TransactionDao;
import com.techgeeknext.dto.ProductDetailDto;
import com.techgeeknext.dto.TransactionDto;
import com.techgeeknext.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;

    //==== ROLE AS USER
    // Fetch Category by idCategory from Database
    public TransactionDao getTransactionById(int idTransaction) {
        return transactionRepository.findByIdTransaction(idTransaction);
    }

    // Insert Product to Database
    public TransactionDao insertTransaction(TransactionDto insertTransaction){
        TransactionDao transaction = new TransactionDao();
        transaction.setAmountTransaction(insertTransaction.getAmountTransaction());
        transaction.setIdUserTrans(insertTransaction.getIdUserTrans());
        transaction.setIdProductTrans(insertTransaction.getIdProductTrans());
        transaction.setIdProductDetailTrans(insertTransaction.getIdProductDetailTrans());
        return transactionRepository.save(transaction);
    }

    // Update Category to Database
    public TransactionDao updateTransaction(TransactionDao transaction, TransactionDto updateTransaction){
        transaction.setAmountTransaction(updateTransaction.getAmountTransaction());
        transaction.setIdUserTrans(updateTransaction.getIdUserTrans());
        transaction.setIdProductTrans(updateTransaction.getIdProductTrans());
        transaction.setIdProductDetailTrans(updateTransaction.getIdProductDetailTrans());
        return transactionRepository.save(transaction);
    }

    // Delete Category from Database
    public void deleteTransaction(TransactionDao transaction){
        transactionRepository.delete(transaction);
    }


    //==== ROLE AS ADMIN
    // Fetch All Users from Database
    public List<TransactionDao> getAllTransaction(){
        return transactionRepository.findAll();
    }

    // Dependency to Category
    public List<TransactionDao> getUserListinTransaction(int idUserTrans){
        return transactionRepository.findAllByIdUserTrans(idUserTrans);
    }

    public List<TransactionDao> getProductListinTransaction(int idProductTrans){
        return transactionRepository.findAllByIdProductTrans(idProductTrans);
    }

    public List<TransactionDao> getProductDetailListinTransaction(int idProductDetailTrans){
        return transactionRepository.findAllByIdProductDetailTrans(idProductDetailTrans);
    }
}
