package com.techgeeknext.service;

import com.techgeeknext.dao.CategoryDao;
import com.techgeeknext.dao.UserDao;
import com.techgeeknext.dto.CategoryDto;
import com.techgeeknext.dto.UserDto;
import com.techgeeknext.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    //==== ROLE AS USER
    // Fetch Category by idCategory from Database
    public CategoryDao getCategoryById(int idCategory) {

        // call user repository to get data from db
        return categoryRepository.findByIdCategory(idCategory);
    }

    // Insert Category to Database
    public CategoryDao insertCategory(CategoryDto insertCategory){
        CategoryDao category = new CategoryDao();
        category.setIdCategory(insertCategory.getIdCategory());
        category.setNameCategory(insertCategory.getNameCategory());
        return categoryRepository.save(category);
    }

    // Update Category to Database
    public CategoryDao updateCategory(CategoryDao category, CategoryDto updateCategory){
        category.setNameCategory(updateCategory.getNameCategory());
        return categoryRepository.save(category);
    }

    // Delete Category from Database
    public void deleteCategory(CategoryDao category){
        categoryRepository.delete(category);
    }



    //==== ROLE AS ADMIN
    // Fetch All Users from Database
    public List<CategoryDao> getAllCategory(){
        return categoryRepository.findAll();
    }
}
