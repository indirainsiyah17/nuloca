package com.techgeeknext.service;

import com.techgeeknext.dao.CategoryDao;
import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dto.CategoryDto;
import com.techgeeknext.dto.ProductDto;
import com.techgeeknext.repository.CategoryRepository;
import com.techgeeknext.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    //==== ROLE AS USER
    // Fetch Category by idCategory from Database
    public ProductDao getProductById(int idProduct) {
        return productRepository.findByIdProduct(idProduct);
    }

    // Insert Product to Database
    public ProductDao insertProduct(ProductDto insertProduct){
        ProductDao product = new ProductDao();
        product.setNameProduct(insertProduct.getNameProduct());
        product.setIdCategory(insertProduct.getIdCategory());
        return productRepository.save(product);
    }

    // Update Category to Database
    public ProductDao updateProduct(ProductDao product, ProductDto updateProduct){
        product.setNameProduct(updateProduct.getNameProduct());
        product.setIdCategory(updateProduct.getIdCategory());
        return productRepository.save(product);
    }

    // Delete Category from Database
    public void deleteProduct(ProductDao product){
        productRepository.delete(product);
    }


    //==== ROLE AS ADMIN
    // Fetch All Users from Database
    public List<ProductDao> getAllProduct(){
        return productRepository.findAll();
    }

    // Dependency to Category
    public List<ProductDao> getproductListinCategory(int idCategory){
        return productRepository.findAllByIdCategory(idCategory);
    }
}
