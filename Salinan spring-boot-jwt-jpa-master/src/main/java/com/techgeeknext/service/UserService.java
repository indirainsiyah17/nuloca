package com.techgeeknext.service;

import com.techgeeknext.dao.UserDao;
import com.techgeeknext.dto.UserDto;
//import com.techgeeknext.repository.ProfileRepository;
import com.techgeeknext.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class UserService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	//==== ROLE AS USER

	// Check Username to User Login
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDao user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}

	// Insert User to Database by Register
	public UserDao save(UserDto user) {
		UserDao newUser = new UserDao();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		return userRepository.save(newUser);
	}

	// Fetch User by idUser from Database
	public UserDao getUserById(int idUser) {

		// call user repository to get data from db
		return userRepository.findByIdUser(idUser);
	}

	// Update User to Database
	public UserDao updateUser(UserDao user, UserDto updateUser){
		user.setEmail(updateUser.getEmail());
		user.setPhoneNumber(updateUser.getPhoneNumber());
		return userRepository.save(user);
	}

	// Delete User from Database
	public void deleteUser(UserDao user){
		userRepository.delete(user);
	}

	//==== ROLE AS ADMIN
	// Fetch All Users from Database
	public List<UserDao> getAllUser(){
		return userRepository.findAll();
	}
}