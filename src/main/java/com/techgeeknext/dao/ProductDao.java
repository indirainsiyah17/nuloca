package com.techgeeknext.dao;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class ProductDao {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id_product;
    private String name_product;
    private int id_category;

    public ProductDao(){}

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }
}
