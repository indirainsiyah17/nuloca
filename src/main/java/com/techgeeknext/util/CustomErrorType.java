package com.techgeeknext.util;

public class CustomErrorType {

    /*
     * This class can return custom JSON Error message on Body
     * */
    private String errorMessage;

    public CustomErrorType(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
