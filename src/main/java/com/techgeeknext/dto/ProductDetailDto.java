package com.techgeeknext.dto;

import java.io.Serializable;

public class ProductDetailDto implements Serializable {
    private int id_product_detail;
    private String name_product_detail;
    private int price;
    private int id_product;

    public int getId_product_detail() {
        return id_product_detail;
    }

    public void setId_product_detail(int id_product_detail) {
        this.id_product_detail = id_product_detail;
    }

    public String getName_product_detail() {
        return name_product_detail;
    }

    public void setName_product_detail(String name_product_detail) {
        this.name_product_detail = name_product_detail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }
}
