package com.techgeeknext.dto;

import java.io.Serializable;

public class TransactionDto implements Serializable {
    private int id_transaction;
    private long amount_transaction;
    private int id_user_trans;
    private int id_product_trans;
    private int id_product_detail;

    public int getId_transaction() {
        return id_transaction;
    }

    public void setId_transaction(int id_transaction) {
        this.id_transaction = id_transaction;
    }

    public long getAmount_transaction() {
        return amount_transaction;
    }

    public void setAmount_transaction(long amount_transaction) {
        this.amount_transaction = amount_transaction;
    }

    public int getId_user_trans() {
        return id_user_trans;
    }

    public void setId_user_trans(int id_user_trans) {
        this.id_user_trans = id_user_trans;
    }

    public int getId_product_trans() {
        return id_product_trans;
    }

    public void setId_product_trans(int id_product_trans) {
        this.id_product_trans = id_product_trans;
    }

    public int getId_product_detail() {
        return id_product_detail;
    }

    public void setId_product_detail(int id_product_detail) {
        this.id_product_detail = id_product_detail;
    }
}
