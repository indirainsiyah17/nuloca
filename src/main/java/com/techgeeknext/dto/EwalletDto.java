package com.techgeeknext.dto;

import java.io.Serializable;

public class EwalletDto implements Serializable {
    private int id_ewallet;
    private long balance;
    private int id_user_ewallet;

    public int getId_ewallet() {
        return id_ewallet;
    }

    public void setId_ewallet(int id_ewallet) {
        this.id_ewallet = id_ewallet;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public int getId_user_ewallet() {
        return id_user_ewallet;
    }

    public void setId_user_ewallet(int id_user_ewallet) {
        this.id_user_ewallet = id_user_ewallet;
    }

    public void topUp(long balance){
        this.balance+=balance;
    }

    public void payment(long balance){
        this.balance-=balance;
    }
}
