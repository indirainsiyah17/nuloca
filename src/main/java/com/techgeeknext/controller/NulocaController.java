package com.techgeeknext.controller;
//import com.techgeeknext.dao.ProfileDao;
import com.techgeeknext.dao.*;
//import com.techgeeknext.repository.ProfileRepository;
import com.techgeeknext.repository.*;
import com.techgeeknext.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin()
public class NulocaController {
    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    UserRepository userRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    ProductDetailRepository productDetailRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    EwalletRepository ewalletRepository;

    //============================================= TESTING =================================================

    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
    public String getEmployees() {
        return "Welcome!";
    }

    /** WARN TO SOLVE
     *
     * (There is a high level bug)
     * All tables on database can not be deleted due to implement "Optional" type rather than "List" type.
     */

    //========================================== USER CONTROLLER ============================================

    /** Note to Solve (There are some bugs)
    * User can open, update and delete other user account by Id_User.
    * It may happen because programmer was not set session management in a proper way.
    */

    //==== ROLE AS USER
    // Open User Profile
    @GetMapping("/user/{id}")
    public ResponseEntity<UserDao> getUserProfile(@PathVariable("id") int id){
        Optional<UserDao> profile = userRepository.findById(id);
        if(profile.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(profile.get(), HttpStatus.OK);
    }

    // Update User Profile
    @PutMapping("/user/{id}")
    public ResponseEntity<UserDao> updateUserProfile(@PathVariable("id") int id_user, @RequestBody UserDao updateUser){
        Optional<UserDao> user = userRepository.findById(id_user);

        if(user.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else{
            UserDao currentUser = user.get();
            currentUser.setEmail(updateUser.getEmail());
            currentUser.setPhone_number(updateUser.getPhone_number());
            return new ResponseEntity<>(userRepository.save(currentUser), HttpStatus.OK);
        }
    }

    // Delete Account User
    @DeleteMapping("/user/{id}")
    public ResponseEntity<UserDao> deleteUserAccount(@PathVariable("id") int id_user){
        Optional<UserDao> user = userRepository.findById(id_user);

        if(user.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }else {
            try{
            userRepository.delete(user.get());
            return new ResponseEntity<>(user.get(), HttpStatus.NO_CONTENT);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
    }

    
    //==== ROLE AS ADMIN
    // Open All User Profile
    @GetMapping("/user/")
    public ResponseEntity<List<UserDao>> getUserProfileList(){
        try{
            List<UserDao> userList = userRepository.findAll();
            if(userList.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(userList, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //========================================== HOME CONTROLLER ============================================

    //==== ROLE AS USER
    // Open All Category in Tagging Product on Home
    @GetMapping("/category/")
    public ResponseEntity<List<CategoryDao>> getCategoryList(){
        try{
            List<CategoryDao> categoryList = categoryRepository.findAll();
            if(categoryList.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(categoryList, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Open All Product on Home Preview
    @GetMapping("/product/")
    public ResponseEntity<List<ProductDao>> getProductList(){
        try{
            List<ProductDao> productList = productRepository.findAll();
            if(productList.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(productList, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //==== ROLE AS ADMIN
    // Insert Category in Tagging Product
    @PostMapping("/category/")
    public ResponseEntity<CategoryDao> insertCategory(@RequestBody CategoryDao category){
        try{
            CategoryDao newCategory = new CategoryDao();
            newCategory.setName_category(category.getName_category());
            categoryRepository.save(newCategory);
            return new ResponseEntity<>(newCategory, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Update Category Tagging Product
    @PutMapping("/category/{id}")
    public ResponseEntity<CategoryDao> updateCategory(@Param("id") int id_category, @RequestBody CategoryDao updateCategory){
        Optional<CategoryDao> category = categoryRepository.findById(id_category);

        if(category.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            CategoryDao currentCategory = category.get();
            currentCategory.setName_category(updateCategory.getName_category());
            categoryRepository.save(currentCategory);
            return new ResponseEntity<>(currentCategory, HttpStatus.OK);
        }
    }

    // Delete Category Tagging Product
    @DeleteMapping("/category/{id}")
    public ResponseEntity<CategoryDao> deleteCategory(@Param("id") int id_category){
        Optional<CategoryDao> category = categoryRepository.findById(id_category);

        if(category.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            try{
                categoryRepository.delete(category.get());
                return new ResponseEntity<>(category.get(), HttpStatus.NO_CONTENT);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
    }

    // Insert Product
    @PostMapping("/product/")
    public ResponseEntity<ProductDao> insertProduct(@RequestBody ProductDao product){
        try{
            ProductDao newProduct = new ProductDao();
            newProduct.setName_product(product.getName_product());
            newProduct.setId_category(product.getId_category());
            productRepository.save(newProduct);
            return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Update Product
    @PutMapping("/product/{id}")
    public ResponseEntity<ProductDao> updateProduct(@Param("id") int id_product, @RequestBody ProductDao updateProduct){
        Optional<ProductDao> product = productRepository.findById(id_product);

        if(product.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            ProductDao currentProduct = product.get();
            currentProduct.setName_product(updateProduct.getName_product());
            currentProduct.setId_category(updateProduct.getId_category());
            productRepository.save(currentProduct);
            return new ResponseEntity<>(currentProduct, HttpStatus.OK);
        }
    }

    // Delete Product
    @DeleteMapping("/product/{id}")
    public ResponseEntity<ProductDao> deleteProduct(@Param("id") int id_product){
        Optional<ProductDao> product = productRepository.findById(id_product);

        if(product.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            try{
                productRepository.delete(product.get());
                return new ResponseEntity<>(product.get(), HttpStatus.NO_CONTENT);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
    }


    //========================================== CATEGORY DETAILS PAGE CONTROLLER ============================================

    /**
     * Should make API for fetch category details.
     * It must be contained array of products.
     * HINT : Related to category and product tables.
     * (GET "/category/{id}")
     */

    //========================================== PRODUCT DETAILS PAGE CONTROLLER ============================================

    /**
     * Should make API for fetch product details.
     * It must be contained array of productsdetails.
     * HINT : Related to product and productdetail tables.
     * (GET "/product/{id}")
     */

    //==== ROLE ADMIN
    // Insert ProductDetail
    @PostMapping("/product/detail/")
    public ResponseEntity<ProductDetailDao> insertProductDetail(@RequestBody ProductDetailDao productDetail){
        try{
            ProductDetailDao newProductDetail = new ProductDetailDao();
            newProductDetail.setName_product_detail(productDetail.getName_product_detail());
            newProductDetail.setPrice(productDetail.getPrice());
            newProductDetail.setId_product(productDetail.getId_product());
            productDetailRepository.save(newProductDetail);
            return new ResponseEntity<>(newProductDetail, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    // Update ProductDetail
    @PutMapping("/product/detail/{id}")
    public ResponseEntity<ProductDetailDao> updateProductDetail(@Param("id") int id_product_detail, @RequestBody ProductDetailDao updateProductDetail){
        Optional<ProductDetailDao> productDetail = productDetailRepository.findById(id_product_detail);

        if(productDetail.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            ProductDetailDao currentProductDetail = productDetail.get();
            currentProductDetail.setName_product_detail(updateProductDetail.getName_product_detail());
            currentProductDetail.setPrice(updateProductDetail.getPrice());
            currentProductDetail.setId_product(updateProductDetail.getId_product());
            productDetailRepository.save(currentProductDetail);
            return new ResponseEntity<>(currentProductDetail, HttpStatus.OK);
        }
    }

    // Delete ProductDetail
    @DeleteMapping("/product/detail/{id}")
    public ResponseEntity<ProductDetailDao> deleteProductDetail(@Param("id") int id_product_detail){
        Optional<ProductDetailDao> productDetail = productDetailRepository.findById(id_product_detail);

        if(productDetail.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            try{
                productDetailRepository.delete(productDetail.get());
                return new ResponseEntity<>(productDetail.get(), HttpStatus.NO_CONTENT);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
    }


    //========================================== MY SUBSCRIPTIONS CONTROLLER ============================================

    /**
     * Should make API for fetch user's subscription products.
     * It must be contained array of product.
     * HINT : Related to user, userproduct and product tables.
     * (GET "/user/subscription/{id}")
     */

    //========================================== TRANSACTION CONTROLLER ============================================

    //==== ROLE USER
    // Insert Transaction
    @PostMapping("/payment/")
    public ResponseEntity<TransactionDao> insertTransactionDetail(@RequestBody TransactionDao transaction){
        try{
            TransactionDao newTransaction = new TransactionDao();
            newTransaction.setAmount_transaction(transaction.getAmount_transaction());
            newTransaction.setId_user_trans(transaction.getId_user_trans());
            newTransaction.setId_product_trans(transaction.getId_product_trans());
            newTransaction.setId_product_detail(transaction.getId_product_detail());
            transactionRepository.save(newTransaction);
            return new ResponseEntity<>(newTransaction, HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }

    //==== ROLE ADMIN
    // Open All Transaction Users
    @GetMapping("/payment/")
    public ResponseEntity<List<TransactionDao>> getTransactionList(){
        try{
            List<TransactionDao> transactionList = transactionRepository.findAll();
            if(transactionList.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(transactionList, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Update Transaction
    @PutMapping("/payment/{id}")
    public ResponseEntity<TransactionDao> updateTransaction(@Param("id") int id_transaction, @RequestBody TransactionDao updateTransaction){
        Optional<TransactionDao> transaction = transactionRepository.findById(id_transaction);

        if(transaction.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            TransactionDao currentTransaction = transaction.get();
            currentTransaction.setAmount_transaction(updateTransaction.getAmount_transaction());
            transactionRepository.save(currentTransaction);
            return new ResponseEntity<>(currentTransaction, HttpStatus.OK);
        }
    }

    // Delete ProductDetail
    @DeleteMapping("/payment/{id}")
    public ResponseEntity<TransactionDao> deleteTransaction(@Param("id") int id_transaction){
        Optional<TransactionDao> transaction = transactionRepository.findById(id_transaction);

        if(transaction.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            try{
                transactionRepository.delete(transaction.get());
                return new ResponseEntity<>(transaction.get(), HttpStatus.NO_CONTENT);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
    }

    //========================================== EWALLET CONTROLLER ============================================

    //==== ROLE USER
    // Insert Balance Ewallet First to Shop in NULOCA
    /**
     * Have not yet decided to push user TopUp their Ewallet (initiate insert ewallet in table) to shop in NULOCA.
     * HINT : Related to ewallet tables.
     * (POST "/ewallet/")
     */

    // TopUp Ewallet
    @PutMapping("/ewallet/topUp/{id}")
    public ResponseEntity<EwalletDao> topUp(@Param("id") int id_ewallet, @RequestBody EwalletDao topUp){
        Optional<EwalletDao> topUpEwallet = ewalletRepository.findById(id_ewallet);

        if(topUpEwallet.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            EwalletDao currentBalance = topUpEwallet.get();
            currentBalance.topUp(topUp.getBalance());
            ewalletRepository.save(currentBalance);
            return new ResponseEntity<>(currentBalance, HttpStatus.OK);
        }
    }

    // Payment Ewallet
    @PutMapping("/ewallet/payment/{id}")
    public ResponseEntity<EwalletDao> payment(@Param("id") int id_ewallet, @RequestBody EwalletDao payment){
        Optional<EwalletDao> topUpEwallet = ewalletRepository.findById(id_ewallet);

        if(topUpEwallet.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            EwalletDao currentBalance = topUpEwallet.get();
            currentBalance.payment(payment.getBalance());
            ewalletRepository.save(currentBalance);
            return new ResponseEntity<>(currentBalance, HttpStatus.OK);
        }
    }


    //==== ROLE ADMIN
    // Open All Ewallet Users
    @GetMapping("/ewallet/")
    public ResponseEntity<List<EwalletDao>> getEwalletList(){
        try{
            List<EwalletDao> ewalletList = ewalletRepository.findAll();
            if(ewalletList.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }else {
                return new ResponseEntity<>(ewalletList, HttpStatus.OK);
            }
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Update Transaction
    @PutMapping("/ewallet/{id}")
    public ResponseEntity<EwalletDao> updateEwallet(@Param("id") int id_ewallet, @RequestBody EwalletDao updateEwallet){
        Optional<EwalletDao> ewallet = ewalletRepository.findById(id_ewallet);

        if(ewallet.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            EwalletDao currentBalance = ewallet.get();
            currentBalance.setBalance(updateEwallet.getBalance());
            ewalletRepository.save(currentBalance);
            return new ResponseEntity<>(currentBalance, HttpStatus.OK);
        }
    }

    // Delete ProductDetail
    @DeleteMapping("/ewallet/{id}")
    public ResponseEntity<EwalletDao> deleteEwallet(@Param("id") int id_ewallet){
        Optional<EwalletDao> ewallet = ewalletRepository.findById(id_ewallet);

        if(ewallet.isEmpty()){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }else {
            try{
                ewalletRepository.delete(ewallet.get());
                return new ResponseEntity<>(ewallet.get(), HttpStatus.NO_CONTENT);
            }catch (Exception e){
                return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
            }
        }
    }
}