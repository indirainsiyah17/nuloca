package com.techgeeknext.repository;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dao.TransactionDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionDao, Integer> {
    List<TransactionDao> findAll();
}
