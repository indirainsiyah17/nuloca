package com.techgeeknext.repository;

import com.techgeeknext.dao.UserProductDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserProductRepository extends CrudRepository<UserProductDao, Integer> {
    UserProductDao findByIduserproduct (int iduserproduct);
    UserProductDao findByIduserrelation (int iduserrelation);
    UserProductDao findByIdproductrelation (int idproductrelation);
    List<UserProductDao> findAllByIduserrelation (int iduserrelation);
}
