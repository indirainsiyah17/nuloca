//package com.techgeeknext.repository;
//
//import com.techgeeknext.dao.CategoryDao;
//import com.techgeeknext.dao.EwalletDao;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public interface EwalletRepository extends CrudRepository<EwalletDao, Integer> {
//    EwalletDao findByIdewallet(int idewallet);
//    List<EwalletDao> findAll();
//}
