package com.techgeeknext.repository;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dao.ProductDetailDao;
import com.techgeeknext.dao.TransactionDao;
import com.techgeeknext.dao.UserDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionDao, Integer> {
    TransactionDao findByIdtransaction(int idtransaction);
    List<TransactionDao> findAll();
    UserDao findByIdusertrans(int idusertrans);
    ProductDao findByIdproducttrans(int idproducttrans);
    ProductDetailDao findByIdproductdetailtrans(int idproductdetailtrans);
}
