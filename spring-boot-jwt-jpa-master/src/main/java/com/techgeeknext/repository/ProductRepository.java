package com.techgeeknext.repository;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dto.ProductDto;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public interface ProductRepository extends CrudRepository<ProductDao, Integer> {
//    EntityManager entityManager = null;
//    EntityTransaction entityTransaction = null;
//    @Modifying
//    @Query(value = "SELECT p.nameProduct FROM product p WHERE p.idCategory=:id",nativeQuery = true)
//    List<ProductDao> productList(@Param("id") int idCategory);
    //List<ProductDao> productList(@Param("id") int id_category);WHERE p.id_category=:id
    //@Transactional
//    @Modifying
//    @Query(value = "SELECT nameproduct FROM product WHERE idcategory =:id", nativeQuery = true)
//    List<ProductDao> listproductcategory(@Param("id") int idcategory);
    ProductDao findByIdproduct(int idproduct);
    List<ProductDao> findAllByIdcategory(int idcategory);
    List<ProductDao> findAll();
}
