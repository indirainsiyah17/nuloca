package com.techgeeknext.dto;

import com.techgeeknext.dao.ProductDao;

import java.io.Serializable;
import java.util.List;

public class UserDto implements Serializable {
    private int iduser;
    private String username;
    private String password;
    private String email;
    private int phonenumber;
    private int ewalletbalance;
    private String profilepict;
    private List<ProductDao> mySubscriptions;
    private String token;

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getEwalletbalance() {
        return ewalletbalance;
    }

    public void setEwalletbalance(int ewalletbalance) {
        this.ewalletbalance = ewalletbalance;
    }

    public String getProfilepict() {
        return profilepict;
    }

    public void setProfilepict(String profilepict) {
        this.profilepict = profilepict;
    }

    public List<ProductDao> getMySubscriptions() {
        return mySubscriptions;
    }

    public void setMySubscriptions(List<ProductDao> mySubscriptions) {
        this.mySubscriptions = mySubscriptions;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
