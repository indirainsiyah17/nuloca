package com.techgeeknext.dto;

import com.techgeeknext.dao.ProductDao;

import java.io.Serializable;
import java.util.List;

public class ProductDto implements Serializable {
    private int idproduct;
    private String nameproduct;
    private int idcategory;
    private String productpict;
    private  List<ProductDao> productList;
    private List<ProductDetailDto> productDetailList;

    public ProductDto(){}

    public ProductDto(String nameproduct){
        this.nameproduct=nameproduct;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public String getNameproduct() {
        return nameproduct;
    }

    public void setNameproduct(String nameproduct) {
        this.nameproduct = nameproduct;
    }

    public int getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(int idcategory) {
        this.idcategory = idcategory;
    }

    public String getProductpict() {
        return productpict;
    }

    public void setProductpict(String productpict) {
        this.productpict = productpict;
    }

    public List<ProductDao> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDao> productList) {
        this.productList = productList;
    }

    public List<ProductDetailDto> getProductDetailList() {
        return productDetailList;
    }

    public void setProductDetailList(List<ProductDetailDto> productDetailList) {
        this.productDetailList = productDetailList;
    }
}
