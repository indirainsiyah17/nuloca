package com.techgeeknext.dto;

import java.io.Serializable;
import java.util.List;

public class UserProductDto implements Serializable {
    private int iduserproduct;
    private int iduserrelation;
    private int idproductrelation;
    private UserDto userSubscription;
    List<UserProductDto> productList;

    public int getIduserproduct() {
        return iduserproduct;
    }

    public void setIduserproduct(int iduserproduct) {
        this.iduserproduct = iduserproduct;
    }

    public int getIduserrelation() {
        return iduserrelation;
    }

    public void setIduserrelation(int iduserrelation) {
        this.iduserrelation = iduserrelation;
    }

    public int getIdproductrelation() {
        return idproductrelation;
    }

    public void setIdproductrelation(int idproductrelation) {
        this.idproductrelation = idproductrelation;
    }

    public UserDto getUserSubscription() {
        return userSubscription;
    }

    public void setUserSubscription(UserDto userSubscription) {
        this.userSubscription = userSubscription;
    }

    public List<UserProductDto> getProductList() {
        return productList;
    }

    public void setProductList(List<UserProductDto> productList) {
        this.productList = productList;
    }
}
