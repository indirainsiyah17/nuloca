package com.techgeeknext.dto;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dao.ProductDetailDao;
import com.techgeeknext.dao.UserDao;

import java.io.Serializable;
import java.util.List;

public class TransactionDto implements Serializable {
    private int idtransaction;
    private long amounttransaction;
    private int idusertrans;
    private int idproducttrans;
    private int idproductdetailtrans;
    private List<UserDao> user;
    private List<ProductDao> product;
    private List<ProductDetailDao> productDetail;

    public int getIdtransaction() {
        return idtransaction;
    }

    public void setIdtransaction(int idtransaction) {
        this.idtransaction = idtransaction;
    }

    public long getAmounttransaction() {
        return amounttransaction;
    }

    public void setAmounttransaction(long amounttransaction) {
        this.amounttransaction = amounttransaction;
    }

    public int getIdusertrans() {
        return idusertrans;
    }

    public void setIdusertrans(int idusertrans) {
        this.idusertrans = idusertrans;
    }

    public int getIdproducttrans() {
        return idproducttrans;
    }

    public void setIdproducttrans(int idproducttrans) {
        this.idproducttrans = idproducttrans;
    }

    public int getIdproductdetailtrans() {
        return idproductdetailtrans;
    }

    public void setIdproductdetailtrans(int idproductdetailtrans) {
        this.idproductdetailtrans = idproductdetailtrans;
    }

    public List<UserDao> getUser() {
        return user;
    }

    public void setUser(List<UserDao> user) {
        this.user = user;
    }

    public List<ProductDao> getProduct() {
        return product;
    }

    public void setProduct(List<ProductDao> product) {
        this.product = product;
    }

    public List<ProductDetailDao> getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(List<ProductDetailDao> productDetail) {
        this.productDetail = productDetail;
    }
}
