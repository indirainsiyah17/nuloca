package com.techgeeknext.dto;

import com.techgeeknext.dao.CategoryDao;
import com.techgeeknext.dao.ProductDao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryDto implements Serializable{
    private int idcategory;
    private String namecategory;
    private String categorypict;
    private List<CategoryDao> categoryList;
    private List<ProductDto> productList;

    public int getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(int idcategory) {
        this.idcategory = idcategory;
    }

    public String getNamecategory() {
        return namecategory;
    }

    public void setNamecategory(String namecategory) {
        this.namecategory = namecategory;
    }

    public List<CategoryDao> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryDao> categoryList) {
        this.categoryList = categoryList;
    }

    public List<ProductDto> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDto> productList) {
        this.productList = productList;
    }

    public String getCategorypict() {
        return categorypict;
    }

    public void setCategorypict(String categorypict) {
        this.categorypict = categorypict;
    }
}
