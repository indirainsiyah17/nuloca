package com.techgeeknext.dto;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dao.ProductDetailDao;

import java.io.Serializable;
import java.util.List;

public class ProductDetailDto implements Serializable {
    private int idproductdetail;
    private String nameproductdetail;
    private int price;
    private int idproduct;
    private List<ProductDetailDao> productDetailList;

    public ProductDetailDto(){}

    public int getIdproductdetail() {
        return idproductdetail;
    }

    public void setIdproductdetail(int idproductdetail) {
        this.idproductdetail = idproductdetail;
    }

    public String getNameproductdetail() {
        return nameproductdetail;
    }

    public void setNameproductdetail(String nameproductdetail) {
        this.nameproductdetail = nameproductdetail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public List<ProductDetailDao> getProductDetailList() {
        return productDetailList;
    }

    public void setProductDetailList(List<ProductDetailDao> productDetailList) {
        this.productDetailList = productDetailList;
    }
}
