//package com.techgeeknext.dao;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "ewallet")
//public class EwalletDao {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private int idewallet;
//    private long balance;
//
//    public int getIdewallet() {
//        return idewallet;
//    }
//
//    public void setIdewallet(int idewallet) {
//        this.idewallet = idewallet;
//    }
//
//    public long getBalance() {
//        return balance;
//    }
//
//    public void setBalance(long balance) {
//        this.balance = balance;
//    }
//
//    public void topUp(long balance){
//        this.balance+=balance;
//    }
//
//    public void payment(long balance){
//        this.balance-=balance;
//    }
//
//}
