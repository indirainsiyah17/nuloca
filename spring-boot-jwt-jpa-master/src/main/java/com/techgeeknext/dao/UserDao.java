package com.techgeeknext.dao;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class UserDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int iduser;
    @Column
    private String username;
    @Column
    @JsonIgnore
    private String password;

    private String email;
    private int phonenumber;
    private int ewalletbalance;
    private String profilepict;

    public UserDao(){}

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(int phonenumber) {
        this.phonenumber = phonenumber;
    }

    public int getEwalletbalance() {
        return ewalletbalance;
    }

    public void setEwalletbalance(int ewalletbalance) {
        this.ewalletbalance = ewalletbalance;
    }

    public String getProfilepict() {
        return profilepict;
    }

    public void setProfilepict(String profilepict) {
        this.profilepict = profilepict;
    }

    public void topUp(long ewalletbalance){
        this.ewalletbalance+=ewalletbalance;
    }

    public void payment(long ewalletbalance){
        this.ewalletbalance-=ewalletbalance;
    }
}

