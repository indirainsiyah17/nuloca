package com.techgeeknext.dao;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class ProductDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idproduct")
    private int idproduct;
    @Column(name = "nameproduct")
    private String nameproduct;
    @Column(name = "idcategory")
    private int idcategory;
    @Column(name = "productpict")
    private String productpict;

    public ProductDao(){}

//    public ProductDao(String nameproduct){
//        this.nameproduct=nameproduct;
//    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public String getNameproduct() {
        return nameproduct;
    }

    public void setNameproduct(String nameproduct) {
        this.nameproduct = nameproduct;
    }

    public int getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(int idcategory) {
        this.idcategory = idcategory;
    }

    public String getProductpict() {
        return productpict;
    }

    public void setProductpict(String productpict) {
        this.productpict = productpict;
    }
}
