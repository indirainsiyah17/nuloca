package com.techgeeknext.dao;

import javax.persistence.*;

@Entity
@Table(name = "category")
public class CategoryDao{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idcategory;
    private String namecategory;
    private String categorypict;

    public CategoryDao(){}

    public int getIdcategory() {
        return idcategory;
    }

    public void setIdcategory(int idcategory) {
        this.idcategory = idcategory;
    }

    public String getNamecategory() {
        return namecategory;
    }

    public void setNamecategory(String namecategory) {
        this.namecategory = namecategory;
    }

    public String getCategorypict() {
        return categorypict;
    }

    public void setCategorypict(String categorypict) {
        this.categorypict = categorypict;
    }
}

