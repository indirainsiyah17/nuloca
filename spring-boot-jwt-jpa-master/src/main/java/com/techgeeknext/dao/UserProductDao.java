package com.techgeeknext.dao;

import javax.persistence.*;

@Entity
@Table(name = "userproduct")
public class UserProductDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int iduserproduct;
    private int iduserrelation;
    private int idproductrelation;

    public int getIduserproduct() {
        return iduserproduct;
    }

    public void setIduserproduct(int iduserproduct) {
        this.iduserproduct = iduserproduct;
    }

    public int getIduserrelation() {
        return iduserrelation;
    }

    public void setIduserrelation(int iduserrelation) {
        this.iduserrelation = iduserrelation;
    }

    public int getIdproductrelation() {
        return idproductrelation;
    }

    public void setIdproductrelation(int idproductrelation) {
        this.idproductrelation = idproductrelation;
    }
}
