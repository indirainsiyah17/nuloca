package com.techgeeknext.dao;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "transaction")
public class TransactionDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idtransaction;
    private long amounttransaction;
    private int idusertrans;
    private int idproducttrans;
    private int idproductdetailtrans;

    public int getIdtransaction() {
        return idtransaction;
    }

    public void setIdtransaction(int idtransaction) {
        this.idtransaction = idtransaction;
    }

    public long getAmounttransaction() {
        return amounttransaction;
    }

    public void setAmounttransaction(long amounttransaction) {
        this.amounttransaction = amounttransaction;
    }

    public int getIdusertrans() {
        return idusertrans;
    }

    public void setIdusertrans(int idusertrans) {
        this.idusertrans = idusertrans;
    }

    public int getIdproducttrans() {
        return idproducttrans;
    }

    public void setIdproducttrans(int idproducttrans) {
        this.idproducttrans = idproducttrans;
    }

    public int getIdproductdetailtrans() {
        return idproductdetailtrans;
    }

    public void setIdproductdetailtrans(int idproductdetailtrans) {
        this.idproductdetailtrans = idproductdetailtrans;
    }

}
