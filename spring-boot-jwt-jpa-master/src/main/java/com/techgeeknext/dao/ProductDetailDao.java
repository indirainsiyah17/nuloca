package com.techgeeknext.dao;

import javax.persistence.*;

@Entity
@Table(name = "productdetail")
public class ProductDetailDao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idproductdetail;
    private String nameproductdetail;
    private int price;
    private int idproduct;

    public int getIdproductdetail() {
        return idproductdetail;
    }

    public void setIdproductdetail(int idproductdetail) {
        this.idproductdetail = idproductdetail;
    }

    public String getNameproductdetail() {
        return nameproductdetail;
    }

    public void setNameproductdetail(String nameproductdetail) {
        this.nameproductdetail = nameproductdetail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }
}
