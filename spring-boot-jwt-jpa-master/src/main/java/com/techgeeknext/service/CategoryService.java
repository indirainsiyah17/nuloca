package com.techgeeknext.service;

import com.techgeeknext.dao.CategoryDao;
import com.techgeeknext.dto.CategoryDto;
import com.techgeeknext.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    /** ROLE AS USER */
    // Fetch Category by idCategory from Database
    public CategoryDao getCategoryById(int idcategory) {
        return categoryRepository.findByIdcategory(idcategory);
    }

    // Insert Category to Database
    public CategoryDao insertCategory(CategoryDto insertCategory){
        CategoryDao category = new CategoryDao();
        category.setIdcategory(insertCategory.getIdcategory());
        category.setNamecategory(insertCategory.getNamecategory());
        category.setCategorypict(insertCategory.getCategorypict());
        return categoryRepository.save(category);
    }

    // Update Category to Database
    public CategoryDao updateCategory(CategoryDao category, CategoryDto updateCategory){
        category.setNamecategory(updateCategory.getNamecategory());
        category.setCategorypict(updateCategory.getCategorypict());
        return categoryRepository.save(category);
    }

    // Delete Category from Database
    public void deleteCategory(CategoryDao category){
        categoryRepository.delete(category);
    }

    //===========================================================================================

    /** ROLE AS ADMIN */
    // Fetch All Categories from Database
    public List<CategoryDao> getAllCategory(){
        return categoryRepository.findAll();
    }
}
