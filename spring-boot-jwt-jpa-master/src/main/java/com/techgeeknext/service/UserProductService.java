package com.techgeeknext.service;

import com.techgeeknext.dao.ProductDetailDao;
import com.techgeeknext.dao.UserDao;
import com.techgeeknext.dao.UserProductDao;
import com.techgeeknext.dto.UserDto;
import com.techgeeknext.repository.UserProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class UserProductService {
    @Autowired
    private UserProductRepository userProductRepository;

    // Insert User to Database by Register
    public UserProductDao saveSubscription(int iduser, int idproduct) {
        UserProductDao newSubscription = new UserProductDao();
        newSubscription.setIduserrelation(iduser);
        newSubscription.setIdproductrelation(idproduct);
        return userProductRepository.save(newSubscription);
    }

    // Fetch User by idUser from Database
    public UserProductDao getUserByIduserrelation(int iduserrelation) {
        return userProductRepository.findByIduserrelation(iduserrelation);
    }

    // Fetch Product by idproductrelation from Database
    public UserProductDao getUserByIdproductrelation(int idproductrelation) {
        return userProductRepository.findByIdproductrelation(idproductrelation);
    }

    // Fetch UserProduct by iduserproduct from Database
    public UserProductDao getUserByIduserproduct(int iduserproduct) {
        return userProductRepository.findByIduserproduct(iduserproduct);
    }

    // Dependency to Product
    public List<UserProductDao> getAllUserByIduser(int iduser){
        return userProductRepository.findAllByIduserrelation(iduser);
    }
}
