package com.techgeeknext.service;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dto.ProductDto;
import com.techgeeknext.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@Transactional
@Service
public class ProductService {
//    private EntityManager entityManager;
//    private EntityTransaction entityTransaction;

    @Autowired
    private ProductRepository productRepository;

    /** ROLE AS USER */
    // Fetch Product by idProduct from Database
    public ProductDao getProductById(int idproduct) {
        return productRepository.findByIdproduct(idproduct);
    }

    // Insert Product to Database
    public ProductDao insertProduct(ProductDto insertProduct){
        ProductDao product = new ProductDao();
        product.setNameproduct(insertProduct.getNameproduct());
        product.setProductpict(insertProduct.getProductpict());
        product.setIdcategory(insertProduct.getIdcategory());
        return productRepository.save(product);
    }

    // Update Product to Database
    public ProductDao updateProduct(ProductDao product, ProductDto updateProduct){
        product.setNameproduct(updateProduct.getNameproduct());
        product.setProductpict(updateProduct.getProductpict());
        product.setIdcategory(updateProduct.getIdcategory());
        return productRepository.save(product);
    }

    // Delete Product from Database
    public void deleteProduct(ProductDao product){
        productRepository.delete(product);
    }

    //===========================================================================================

    /** ROLE AS ADMIN */
    // Fetch All Products from Database
    public List<ProductDao> getAllProduct(){
        return productRepository.findAll();
    }

    //===========================================================================================

    // Dependency to Category
    public List<ProductDao> getproductListinCategory(int idcategory){
        //return entityManager.createNativeQuery("select nameproduct from product where idcategory=:idcategory").setParameter("idcategory", idcategory).getResultList();
        return productRepository.findAllByIdcategory(idcategory);
    }
}
