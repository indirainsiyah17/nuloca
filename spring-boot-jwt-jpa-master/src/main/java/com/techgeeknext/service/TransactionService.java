package com.techgeeknext.service;

import com.techgeeknext.dao.ProductDao;
import com.techgeeknext.dao.ProductDetailDao;
import com.techgeeknext.dao.TransactionDao;
import com.techgeeknext.dao.UserDao;
import com.techgeeknext.dto.TransactionDto;
import com.techgeeknext.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;

    /** ROLE AS USER */
    // Fetch Transaction by idTransaction from Database
    public TransactionDao getTransactionById(int idtransaction) {
        return transactionRepository.findByIdtransaction(idtransaction);
    }

    // Insert Transaction to Database
    public TransactionDao insertTransaction(TransactionDto insertTransaction){
        TransactionDao transaction = new TransactionDao();
        transaction.setAmounttransaction(insertTransaction.getAmounttransaction());
        transaction.setIdusertrans(insertTransaction.getIdusertrans());
        transaction.setIdproducttrans(insertTransaction.getIdproducttrans());
        transaction.setIdproductdetailtrans(insertTransaction.getIdproductdetailtrans());
        return transactionRepository.save(transaction);
    }

    // UpdateTransaction to Database
    public TransactionDao updateTransaction(TransactionDao transaction, TransactionDto updateTransaction){
        transaction.setAmounttransaction(updateTransaction.getAmounttransaction());
        transaction.setIdusertrans(updateTransaction.getIdusertrans());
        transaction.setIdproducttrans(updateTransaction.getIdproducttrans());
        transaction.setIdproductdetailtrans(updateTransaction.getIdproductdetailtrans());
        return transactionRepository.save(transaction);
    }

    // Delete Transaction from Database
    public void deleteTransaction(TransactionDao transaction){
        transactionRepository.delete(transaction);
    }

    //===========================================================================================

    /** ROLE AS ADMIN */
    // Fetch All Transaction from Database
    public List<TransactionDao> getAllTransaction(){
        return transactionRepository.findAll();
    }
}








//    // Dependency to User
//    public UserDao getUserinTransaction(int idusertrans){
//        return transactionRepository.findByIdusertrans(idusertrans);
//    }
//
//    // Dependency to Product
//    public ProductDao getProductinTransaction(int idproducttrans){
//        return transactionRepository.findByIdproducttrans(idproducttrans);
//    }
//
//    // Dependency to Product Detail
//    public ProductDetailDao getProductDetailinTransaction(int idproductdetailtrans){
//        return transactionRepository.findByIdproductdetailtrans(idproductdetailtrans);
//    }