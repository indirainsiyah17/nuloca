package com.techgeeknext.service;

import com.techgeeknext.dao.ProductDetailDao;
import com.techgeeknext.repository.ProductDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ProductDetailService {
    @Autowired
    private ProductDetailRepository productDetailRepository;

    /** ROLE AS USER */
    // Fetch Product Detail by idProduct Detail from Database
    public ProductDetailDao getProductDetailById(int idproductdetail) {
        return productDetailRepository.findByIdproductdetail(idproductdetail);
    }

    // Insert Product Detail to Database
    public ProductDetailDao insertProductDetail(com.techgeeknext.dto.ProductDetailDto insertProductDetail){
        ProductDetailDao productDetail = new ProductDetailDao();
        productDetail.setNameproductdetail(insertProductDetail.getNameproductdetail());
        productDetail.setPrice(insertProductDetail.getPrice());
        productDetail.setIdproduct(insertProductDetail.getIdproduct());
        return productDetailRepository.save(productDetail);
    }

    // Update Product Detail to Database
    public ProductDetailDao updateProductDetail(ProductDetailDao productDetail, com.techgeeknext.dto.ProductDetailDto updateProductDetail){
        productDetail.setNameproductdetail(updateProductDetail.getNameproductdetail());
        productDetail.setPrice(updateProductDetail.getPrice());
        productDetail.setIdproduct(updateProductDetail.getIdproduct());
        return productDetailRepository.save(productDetail);
    }

    // Delete Product Detail from Database
    public void deleteProductDetail(ProductDetailDao productDetail){
        productDetailRepository.delete(productDetail);
    }

    //===========================================================================================

    /** ROLE AS ADMIN */
    // Fetch All Product Detail from Database
    public List<ProductDetailDao> getAllProductDetail(){
        return productDetailRepository.findAll();
    }

    //===========================================================================================

    // Dependency to Product
    public List<ProductDetailDao> getproductDetailListinProduct(int idproduct){
        return productDetailRepository.findAllByIdproduct(idproduct);
    }
}
