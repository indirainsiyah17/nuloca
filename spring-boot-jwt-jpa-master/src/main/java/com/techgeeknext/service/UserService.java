package com.techgeeknext.service;

import com.techgeeknext.dao.TransactionDao;
import com.techgeeknext.dao.UserDao;
import com.techgeeknext.dto.UserDto;
import com.techgeeknext.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Transactional
@Service
public class UserService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	/** ROLE AS USER */
	// Check Username to User Login
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDao user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
				new ArrayList<>());
	}

	// Insert User to Database by Register
	public UserDao save(UserDto user) {
		UserDao newUser = new UserDao();
		newUser.setUsername(user.getUsername());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setEmail(user.getEmail());
		newUser.setProfilepict("");
		return userRepository.save(newUser);
	}

	// Fetch User by idUser from Database
	public UserDao getUserById(int iduser) {
		return userRepository.findByIduser(iduser);
	}

	public UserDao getUserByUsername(String username){
		return userRepository.findByUsername(username);
	}

	// Update User to Database
	public UserDao updateUser(UserDao user, UserDto updateUser){
		user.setUsername(updateUser.getUsername());
		user.setEmail(updateUser.getEmail());
		user.setPhonenumber(updateUser.getPhonenumber());
		user.setProfilepict(updateUser.getProfilepict());
		return userRepository.save(user);
	}

	// Delete User from Database
	public void deleteUser(UserDao user){
		userRepository.delete(user);
	}

	// Update Ewallet by TopUp to Database
	public UserDao topUpEwallet(UserDao topUp, UserDto currentEwallet){
		topUp.topUp(currentEwallet.getEwalletbalance());
		return userRepository.save(topUp);
	}

	// Update Ewallet by Payment to Database
	public UserDao paymentEwallet(UserDao currentEwallet, TransactionDao paymentTrans){
		currentEwallet.payment(paymentTrans.getAmounttransaction());
		return userRepository.save(currentEwallet);
	}

	//===========================================================================================

	/** ROLE AS ADMIN */
	// Fetch All Users from Database
	public List<UserDao> getAllUser(){
		return userRepository.findAll();
	}
}





//	// Update User to Database
//	public UserDao registerEwallet(UserDao user, int idewallet){
//		user.setEwalletbalance(idewallet);
//		return userRepository.save(user);
//	}

//	public EwalletDao getUserByIdewallet(int idewallet){
//		return userRepository.findByIdewallet(idewallet);
//	}