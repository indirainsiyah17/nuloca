package com.techgeeknext.util;

public class CustomSuccessType {
    /*
     * This class can return custom JSON Error message on Body
     * */
    private String successMessage;

    public CustomSuccessType(String successMessage){
        this.successMessage = successMessage;
    }

    public String getSuccessMessage() {
        return successMessage;
    }
}
